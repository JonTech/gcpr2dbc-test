package com.jt.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import reactor.core.publisher.Flux;

@RestController
public class DemoController {

    //Inject Repo here
    @Autowired
    private DemoService demoService;

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public String hello() {
        return "Hello from Demo Controller";
    }

    @GetMapping("/findAll")
    @ResponseStatus(HttpStatus.OK)
    public Flux<DemoEntity> findAll() {

        System.out.println("Returning stuff");

        return this.demoService.findAll();
    }

    @GetMapping("/save")
    @ResponseStatus(HttpStatus.OK)
    public void save() {

        System.out.println("Returning stuff");
        this.demoService.save(new DemoEntity(1l, "SPM"));
        System.out.println("SAVED!");
    }



}
