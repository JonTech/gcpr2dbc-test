package com.jt.demo;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
@Component
public interface DemoRepository {

    Flux<DemoEntity> findAll();

    void save(DemoEntity demoEntity);

}
