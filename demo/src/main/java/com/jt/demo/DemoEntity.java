package com.jt.demo;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "demos")
public class DemoEntity {

    @Id
    @Column
    private Long id;

    @Column(value = "content")
    private String content;
}
