package com.jt.demo;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class DemoService implements DemoRepository {

    private DemoRepository demoRepository;

    @Override
    public Flux<DemoEntity> findAll() {
        return this.demoRepository.findAll();
    }

    @Override
    public void save(DemoEntity demoEntity) {
        this.demoRepository.save(demoEntity);
    }
}
